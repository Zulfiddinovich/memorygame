package uz.gita.gamememory.presentation.viewmodel

import androidx.lifecycle.LiveData
import uz.gita.gamememory.data.model.LevelEnum

interface LevelViewModel {
    val openGameScreenLiveData: LiveData<LevelEnum>

    fun openGameScreen(level: LevelEnum)
}