package uz.gita.gamememory.presentation.viewmodel

import androidx.lifecycle.LiveData

interface SplashViewModel {
    val openNextScreen: LiveData<Unit>
}