package uz.gita.gamememory.presentation.viewmodel

import android.content.Context
import android.widget.ImageView
import androidx.lifecycle.LiveData
import uz.gita.gamememory.data.model.GameData
import uz.gita.gamememory.data.model.LevelEnum
import uz.gita.gamememory.data.model.SelectedFoundData

interface GameViewModel {
    val allGameDataLiveData: LiveData<List<GameData>>
    val twoSelectedFoundLiveData: LiveData<SelectedFoundData>

    fun loadData(level: LevelEnum)
//    fun openClickImage(image: ImageView, context: Context)
//    fun closeClickImage(image: ImageView, context: Context)
//    fun itemClicked(image: ImageView, context: Context)

    fun checkTwoSelected(firstImage: ImageView, secondImage: ImageView)

    fun getLevel(): Int

    fun setLevel(level: Int)

    fun isMusicOn(): Boolean

    fun isMusicOn(bool: Boolean)

    fun getAttempts(): Int

    fun setAttempts(attempt: Int)
}