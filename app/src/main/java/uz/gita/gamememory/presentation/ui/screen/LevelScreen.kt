package uz.gita.gamememory.presentation.ui.screen

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.gita.gamememory.R
import uz.gita.gamememory.data.model.LevelEnum
import uz.gita.gamememory.databinding.ScreenLevelBinding
import uz.gita.gamememory.presentation.viewmodel.LevelViewModel
import uz.gita.gamememory.presentation.viewmodel.impl.LevelViewModelImpl

@AndroidEntryPoint
class LevelScreen : Fragment(R.layout.screen_level) {
    private val binding by viewBinding(ScreenLevelBinding::bind)
    private val viewModel: LevelViewModel by viewModels<LevelViewModelImpl>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.easy.setOnClickListener { viewModel.openGameScreen(LevelEnum.EASY) }
        binding.medium.setOnClickListener { viewModel.openGameScreen(LevelEnum.MEDIUM) }
        binding.hard.setOnClickListener { viewModel.openGameScreen(LevelEnum.HARD) }

        viewModel.openGameScreenLiveData.observe(this, openGameScreenObserver)
    }

    private val openGameScreenObserver = Observer<LevelEnum> {
        findNavController().navigate(LevelScreenDirections.actionLevelScreenToGameScreen(it))
    }
}