package uz.gita.gamememory.presentation.viewmodel.impl

import android.content.Context
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.gita.gamememory.R
import uz.gita.gamememory.data.model.GameData
import uz.gita.gamememory.data.model.LevelEnum
import uz.gita.gamememory.data.model.SelectedFoundData
import uz.gita.gamememory.domain.usecase.AllDataUseCase
import uz.gita.gamememory.presentation.viewmodel.GameViewModel
import javax.inject.Inject

@HiltViewModel
class GameViewModelImpl @Inject constructor(
    private val useCase: AllDataUseCase
) : ViewModel(), GameViewModel {
    private val attemptLose = 1
    override val allGameDataLiveData = MutableLiveData<List<GameData>>()
    override val twoSelectedFoundLiveData = MutableLiveData<SelectedFoundData>()

    override fun loadData(level: LevelEnum) {
        useCase.getDataByLevel(level).onEach {
            allGameDataLiveData.value = it
        }.launchIn(viewModelScope)
    }


    override fun checkTwoSelected(firstImage: ImageView, secondImage: ImageView) {
       if  ((firstImage.tag as GameData).id == (secondImage.tag as GameData).id) {
           twoSelectedFoundLiveData.value = SelectedFoundData(true, firstImage, secondImage)
       } else {
           setAttempts(getAttempts() - attemptLose)
           twoSelectedFoundLiveData.value = SelectedFoundData(false, firstImage, secondImage)
       }
    }

    override fun getLevel(): Int = useCase.getLevel()

    override fun setLevel(level: Int) {
        useCase.setLevel(level)
    }

    override fun isMusicOn(): Boolean = useCase.isMusicOn()

    override fun isMusicOn(bool: Boolean) = useCase.isMusicOn(bool)

    override fun getAttempts(): Int = useCase.getAttempts()

    override fun setAttempts(attempt: Int) {
        useCase.setAttempts(attempt)
    }

}