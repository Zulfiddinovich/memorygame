package uz.gita.gamememory.presentation.ui.screen

import android.animation.ValueAnimator
import android.app.AlertDialog
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import android.view.animation.AnticipateOvershootInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import uz.gita.gamememory.R
import uz.gita.gamememory.data.model.GameData
import uz.gita.gamememory.data.model.LevelEnum
import uz.gita.gamememory.data.model.SelectedFoundData
import uz.gita.gamememory.databinding.ScreenGameBinding
import uz.gita.gamememory.presentation.viewmodel.GameViewModel
import uz.gita.gamememory.presentation.viewmodel.impl.GameViewModelImpl
import uz.gita.gamememory.utils.gone
import uz.gita.gamememory.utils.scope

@AndroidEntryPoint
class GameScreen : Fragment(R.layout.screen_game) {
    private val binding by viewBinding(ScreenGameBinding::bind)
    private val viewModel: GameViewModel by viewModels<GameViewModelImpl>()
    private val args: GameScreenArgs by navArgs()
    private var image1: ImageView? = null
    private var image2: ImageView? = null
    private var count = 0
    private var matrix = 0
    private var level: LevelEnum = LevelEnum.EASY
    private var mediaPlayer: MediaPlayer? = null
    private var quickMusicOn = true // musicni qayta tez yoqqanda ovozi chiqishi uchun
    private var clapsMedia: MediaPlayer? = null
    private var unSuccessMedia: MediaPlayer? = null
    private var _height = 0
    private var _width = 0
    private var gainAttempt = 4
    private var step = 0
    private val views = ArrayList<ImageView>()
    private var showAndHideJob: Job? = null
    private var itemClickedJob: Job? = null
    private var isRestartAllowed = false  // restart berishda null likdan qochish, ya`ni hamma corotine scoplar yopilishini kutadi
    private var isRestartAllowed2 = true  // 1ta 1ta bosganda nullikdan qutqarish uchun, asosan kutish yechim bo`ldi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        args.level.apply {
            level = this
            matrix = x * y
        }

        mediaPlayer = MediaPlayer.create(requireContext(), R.raw.game_background_music)
        mediaPlayer?.isLooping = true
        mediaPlayer?.setVolume(0.3f, 0.3f)

        clapsMedia = MediaPlayer.create(requireContext(), R.raw.clapping_sound)
        clapsMedia?.setVolume(0.3f, 0.3f)

        unSuccessMedia = MediaPlayer.create(requireContext(), R.raw.lost_game)
        unSuccessMedia?.setVolume(0.3f, 0.3f)


        viewModel.setLevel(1)
        when(level){
            LevelEnum.EASY -> {
                viewModel.setAttempts(10)
                gainAttempt = 4
            }
            LevelEnum.MEDIUM -> {
                viewModel.setAttempts(18)
                gainAttempt = 6
            }
            LevelEnum.HARD -> {
                viewModel.setAttempts(26)
                gainAttempt = 8
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = binding.scope {

        main.post {
            _height = main.height / (level.y + 2)
            _width = main.width / (level.x) - 30
            container.layoutParams.apply {
                height = level.y * _height
                width = level.x * _width
            }
            loadView()
            viewModel.loadData(level)
        }
        reload.setOnClickListener {
            restart()
        }

        if (viewModel.isMusicOn()) {
            mediaPlayer?.start()
            Glide.with(binding.menu).load(R.drawable.music_on).into(binding.menu)
        } else {
            Glide.with(binding.menu).load(R.drawable.music_off).into(binding.menu)
        }

        menu.setOnClickListener {
            viewModel.isMusicOn(!viewModel.isMusicOn())
            if (viewModel.isMusicOn()) {
                mediaPlayer?.start()
                quickMusicOn = true
                Glide.with(binding.menu).load(R.drawable.music_on).into(binding.menu)
            }
            else {
                quickMusicOn = false
                softStopMedia()
                Glide.with(binding.menu).load(R.drawable.music_off).into(binding.menu)
            }
        }

        viewModel.allGameDataLiveData.observe(viewLifecycleOwner, allGameDataObserver)
        viewModel.twoSelectedFoundLiveData.observe(viewLifecycleOwner, twoSelectedFoundObserver)
    }

    private val allGameDataObserver = Observer<List<GameData>> { list ->
        for (i in list.indices) {
            views[i].apply {
                tag = list[i]
                setOnClickListener {
                    itemClicked(it as ImageView)
                }
            }
        }
    }

    private val twoSelectedFoundObserver = Observer<SelectedFoundData> {
        lifecycleScope.launch {
            if (it.bool) {
                it.image1.animate().setDuration(200).alpha(0f).withEndAction {
                    it.image1.gone()
                }
                views.remove(it.image1)
                it.image2.animate().setDuration(200).alpha(0f).withEndAction {
                    it.image2.gone()
                }
                views.remove(it.image2)
                isRestartAllowed = true
            } else {
                closeClickImage(it.image1)
                closeClickImage(it.image2)
                if (viewModel.getAttempts() == 0) {
                    softStopMedia()
                    attemptOverDialog().show()
                    unSuccessMedia?.start()
                }
            }
                delay(400)
                if (views.size == 0) {
                    if (viewModel.getLevel() == 10) {
                        softStopMedia()
                        stageFinishedDialog().show()
                        clapsMedia?.start()
                    } else {
                        softStopMedia()
                        nextLevelDialog().show()
                        clapsMedia?.start()
                    }
                }

            binding.attempt.text = "${viewModel.getAttempts()}"
        }
    }

    private fun softStopMedia() {
        val va = ValueAnimator.ofFloat(0.3f, 0f)
        va.setDuration(700)
        va.addUpdateListener(object: ValueAnimator.AnimatorUpdateListener{
            override fun onAnimationUpdate(animation: ValueAnimator) {
                mediaPlayer?.setVolume(animation.animatedValue as Float, animation.animatedValue as Float)
                if (animation.animatedValue == 0.0f) {
                    mediaPlayer?.pause()
                    mediaPlayer?.setVolume(0.3f, 0.3f)
                }
                if (quickMusicOn && animation.animatedValue != 0.0f) {
                    animation.removeUpdateListener(this)
                    mediaPlayer?.setVolume(0.3f, 0.3f)
                }
            }
        })
        va.start()
    }

    private fun nextLevelDialog(): AlertDialog{
        val dialogBuilder = AlertDialog.Builder(requireContext(), R.style.dialog_style)
        dialogBuilder.setTitle("The next level is yours!")
        dialogBuilder.setPositiveButton("Next"){dialog, _->
            viewModel.setLevel(viewModel.getLevel() + 1)
            viewModel.setAttempts(viewModel.getAttempts() + gainAttempt)
            restart()
            dialog.dismiss()
        }.setNegativeButton("Go main"){dialog, _ ->
            findNavController().popBackStack()
            dialog.dismiss()
        }.setCancelable(false)

        return dialogBuilder.create()
    }

    private fun attemptOverDialog(): AlertDialog{
        val dialogBuilder = AlertDialog.Builder(requireContext(), R.style.dialog_style)
        dialogBuilder.setTitle("You lost the game")
        dialogBuilder.setPositiveButton("Go main"){dialog, _->
//            viewModel.setLevel(viewModel.getLevel() + 1)
//            viewModel.setAttempts(viewModel.getAttempts() + gainAttempt)
            findNavController().popBackStack()
            dialog.dismiss()
        }.setCancelable(false)
        return dialogBuilder.create()
    }

    private fun stageFinishedDialog(): AlertDialog{
        val dialogBuilder = AlertDialog.Builder(requireContext(), R.style.dialog_style)
        dialogBuilder.setTitle("You won all stages! You can choose harder level")
        dialogBuilder.setPositiveButton("Ok"){ dialog, _->
            findNavController().popBackStack()
            dialog.dismiss()
        }.setCancelable(false)

        return dialogBuilder.create()
    }

    private fun loadView() {
        binding.levelText.text = "${viewModel.getLevel()}/10"
        binding.attempt.text = "${viewModel.getAttempts()}"
        step = 0
        binding.step.text = step.toString()

        for (i in 0 until level.x) {
            for (j in 0 until level.y) {
                val image = ImageView(requireContext())
                val cont = RelativeLayout(requireContext())
                cont.addView(image)
                binding.container.addView(cont)
                val cont_lp = cont.layoutParams as RelativeLayout.LayoutParams
                cont_lp.apply {
                    height = _height + 5
                    width = _width + 5
                }
                cont.layoutParams = cont_lp


                val lp = RelativeLayout.LayoutParams(image.layoutParams)
                lp.apply {
                    height = _height
                    width = _width
                }
                lp.setMargins(15, 15, 15, 15)
                image.layoutParams = lp



                cont.x = i * _width.toFloat()
                cont.y = j * _height.toFloat()
                image.scaleType = ImageView.ScaleType.CENTER_CROP
                Glide.with(image).load(R.drawable.cards_back).into(image)
                views.add(image)

                showAndHideJob = lifecycleScope.launch{
                    delay(500)
                    when (level) {
                        LevelEnum.EASY -> showAndHide(1000, 50)
                        LevelEnum.MEDIUM -> showAndHide(3000, 50)
                        LevelEnum.HARD -> showAndHide(5000, 50)
                    }
                }
            }
        }
    }

    private suspend fun showAndHide(duration: Long, sequenceDuration: Long){
        isRestartAllowed = false
        views.forEachIndexed { index, image ->
            lifecycleScope.launch {
                openClickImage(image)
            }
            /*image.animate().setDuration(200).rotationY(90f).withEndAction {
                Glide.with(image).load((image.tag as GameData).image).into(image)
                image.animate().setDuration(200).rotationY(180f)
                    .setInterpolator(DecelerateInterpolator()).withEndAction {
//                Toast.makeText(requireContext(), "end", Toast.LENGTH_SHORT).show()
                }
            }.start()*/
            delay(sequenceDuration)
            if (index == views.size - 1) {
                delay(duration)
                views.forEach { img ->
                    lifecycleScope.launch {
                        closeClickImage(img)
                    }
                    /*img.animate().setDuration(200).rotationY(90f).withEndAction {
                        Glide.with(img).load(R.drawable.cards_back).into(img)
                        img.animate().setDuration(200).rotationY(0f)
                            .setInterpolator(DecelerateInterpolator()).withEndAction {
//                          Toast.makeText(requireContext(), "end", Toast.LENGTH_SHORT).show()

                        }
                    }.start()*/
                }
                isRestartAllowed = true
            }
        }
    }

    private fun restart(){
        if (isRestartAllowed && isRestartAllowed2){
            isRestartAllowed = false
            count = 0
            binding.container.removeAllViews()
            views.clear()
            image1 = null
            image2 = null
            showAndHideJob?.cancel()
            showAndHideJob = null
            itemClickedJob?.cancel()
            itemClickedJob = null
            loadView()
            viewModel.loadData(level)
        }
    }

    private suspend fun openClickImage(image: ImageView) {
        isRestartAllowed2 = false
        ValueAnimator.ofFloat(0f, 180f).apply {
            addUpdateListener {
                image.rotationY = animatedValue as Float
                if (it.animatedValue == 180f) isRestartAllowed2 = true
            }
            duration = 1000L
            interpolator = AnticipateOvershootInterpolator()
            this.start()
        }
        delay(500L)
        Glide.with(image).load((image.tag as GameData).image).into(image)



        /*image.animate().setDuration(200).rotationY(90f).withEndAction {
            Glide.with(image).load((image.tag as GameData).image).into(image)
            image.animate().setDuration(200).rotationY(180f).setInterpolator(DecelerateInterpolator()).withEndAction {
//                Toast.makeText(requireContext(), "end", Toast.LENGTH_SHORT).show()
                isRestartAllowed2 = true
            }
        }.start()*/
    }

    private suspend fun closeClickImage(image: ImageView) {
        isRestartAllowed2 = false
        ValueAnimator.ofFloat(180f, 360f).apply {
            addUpdateListener {
                image.rotationY = animatedValue as Float
                if (it.animatedValue == 360f) isRestartAllowed2 = true
            }
            duration = 1000L
            interpolator = AnticipateOvershootInterpolator()
            this.start()
        }
        delay(500L)
        Glide.with(image).load(R.drawable.cards_back).into(image)

        /*isRestartAllowed2 = false
        image.animate().setDuration(200).rotationY(90f).withEndAction {
            Glide.with(image).load(R.drawable.cards_back).into(image)
            image.animate().setDuration(200).rotationY(0f).setInterpolator(DecelerateInterpolator()).withEndAction {
//                Toast.makeText(requireContext(), "end", Toast.LENGTH_SHORT).show()
                isRestartAllowed2 = true
            }
        }.start()*/

    }

    private fun itemClicked(image: ImageView) {
        if (isRestartAllowed) {
            if (image.rotationY < 1f || image.rotationY > 359f && count < 3) {
                itemClickedJob = lifecycleScope.launch {
                    step++
                    binding.step.text = step.toString()
                    if (count == 0) {
                        count = 1
                        image1 = image
                        openClickImage(image)
                    } else if (image != image1 && count == 1) {
                        count = 2
                        image2 = image
                        openClickImage(image)
                        delay(1000)
                        viewModel.checkTwoSelected(image1!!, image2!!)
                        count = 0
                    } else {
                        // 3rd image clicked
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        softStopMedia()
        clapsMedia?.pause()
        unSuccessMedia?.pause()
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.isMusicOn()) mediaPlayer?.start()
    }

    override fun onDestroy() {
        mediaPlayer?.stop()
        mediaPlayer = null

        clapsMedia?.stop()
        clapsMedia = null

        unSuccessMedia?.stop()
        unSuccessMedia = null
        super.onDestroy()
    }

}