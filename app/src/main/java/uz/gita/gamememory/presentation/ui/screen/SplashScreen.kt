package uz.gita.gamememory.presentation.ui.screen

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import uz.gita.gamememory.R
import uz.gita.gamememory.databinding.ScreenSplashBinding
import uz.gita.gamememory.presentation.viewmodel.SplashViewModel
import uz.gita.gamememory.presentation.viewmodel.impl.SplashViewModelImpl

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashScreen : Fragment(R.layout.screen_splash) {
    private val binding by viewBinding(ScreenSplashBinding::bind)
    private val viewModel: SplashViewModel by viewModels<SplashViewModelImpl>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.openNextScreen.observe(viewLifecycleOwner, openNextObserver)
    }

    private val openNextObserver = Observer<Unit> {
        findNavController().navigate(SplashScreenDirections.actionSplashScreenToLevelScreen())
    }
}