package uz.gita.gamememory.data.model

enum class LevelEnum(val x: Int, val y: Int) {
    EASY(3, 4),
    MEDIUM(4, 6),
    HARD(6, 8)
}