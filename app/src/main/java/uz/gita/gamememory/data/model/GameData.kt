package uz.gita.gamememory.data.model

import android.graphics.drawable.Drawable

data class GameData(
    val id: Int,
    val image: Drawable
)
