package uz.gita.gamememory.domain.usecase

import kotlinx.coroutines.flow.Flow
import uz.gita.gamememory.data.model.GameData
import uz.gita.gamememory.data.model.LevelEnum

interface AllDataUseCase {
    fun getDataByLevel(level: LevelEnum): Flow<List<GameData>>

    fun getLevel(): Int

    fun setLevel(level: Int)

    fun getAttempts(): Int

    fun setAttempts(attempt: Int)

    fun isMusicOn(): Boolean

    fun isMusicOn(bool: Boolean)
}