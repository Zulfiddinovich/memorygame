package uz.gita.gamememory.domain.repository.impl

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import uz.gita.gamememory.data.model.GameData
import uz.gita.gamememory.data.model.LevelEnum
import uz.gita.gamememory.data.source.local.sharedPref.SharedPref
import uz.gita.gamememory.domain.repository.AppRepository
import uz.gita.gamememory.utils.getFromAssets
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val pref : SharedPref
) : AppRepository {
    private val list = ArrayList<GameData>()

    init { load() }

    private fun load() {
        list.add(GameData(1, getFromAssets(context,"imgs/animal1.webp")))
        list.add(GameData(2, getFromAssets(context,"imgs/animal2.webp")))
        list.add(GameData(3, getFromAssets(context,"imgs/animal3.webp")))
        list.add(GameData(4, getFromAssets(context,"imgs/animal4.webp")))
        list.add(GameData(5, getFromAssets(context,"imgs/animal5.webp")))
        list.add(GameData(6, getFromAssets(context,"imgs/animal6.webp")))
        list.add(GameData(7, getFromAssets(context,"imgs/animal7.webp")))
        list.add(GameData(8, getFromAssets(context,"imgs/animal8.webp")))
        list.add(GameData(9, getFromAssets(context,"imgs/bird1.webp")))
        list.add(GameData(10, getFromAssets(context,"imgs/bird2.webp")))
        list.add(GameData(11, getFromAssets(context,"imgs/bird3.webp")))
        list.add(GameData(12, getFromAssets(context,"imgs/bird4.webp")))
        list.add(GameData(13, getFromAssets(context,"imgs/bird5.webp")))
        list.add(GameData(14, getFromAssets(context,"imgs/bird6.webp")))
        list.add(GameData(15, getFromAssets(context,"imgs/bird7.webp")))
        list.add(GameData(16, getFromAssets(context,"imgs/bird8.webp")))
        list.add(GameData(17, getFromAssets(context,"imgs/building1.webp")))
        list.add(GameData(18, getFromAssets(context,"imgs/building2.webp")))
        list.add(GameData(19, getFromAssets(context,"imgs/building3.webp")))
        list.add(GameData(20, getFromAssets(context,"imgs/building4.webp")))
        list.add(GameData(21, getFromAssets(context,"imgs/building5.webp")))
        list.add(GameData(22, getFromAssets(context,"imgs/building6.webp")))
        list.add(GameData(23, getFromAssets(context,"imgs/building7.webp")))
        list.add(GameData(24, getFromAssets(context,"imgs/building8.webp")))
        list.add(GameData(25, getFromAssets(context,"imgs/city1.webp")))
        list.add(GameData(26, getFromAssets(context,"imgs/city2.webp")))
        list.add(GameData(27, getFromAssets(context,"imgs/city3.webp")))
        list.add(GameData(28, getFromAssets(context,"imgs/city4.webp")))
        list.add(GameData(28, getFromAssets(context,"imgs/city5.webp")))
        list.add(GameData(30, getFromAssets(context,"imgs/city6.webp")))
        list.add(GameData(31, getFromAssets(context,"imgs/city7.webp")))
        list.add(GameData(32, getFromAssets(context,"imgs/city8.webp")))
        list.add(GameData(33, getFromAssets(context,"imgs/flower1.webp")))
        list.add(GameData(34, getFromAssets(context,"imgs/flower2.webp")))
        list.add(GameData(35, getFromAssets(context,"imgs/flower3.webp")))
        list.add(GameData(36, getFromAssets(context,"imgs/flower4.webp")))
        list.add(GameData(37, getFromAssets(context,"imgs/flower5.webp")))
        list.add(GameData(38, getFromAssets(context,"imgs/flower6.webp")))
        list.add(GameData(39, getFromAssets(context,"imgs/flower7.webp")))
        list.add(GameData(40, getFromAssets(context,"imgs/flower8.webp")))
        list.add(GameData(41, getFromAssets(context,"imgs/forest1.webp")))
        list.add(GameData(42, getFromAssets(context,"imgs/forest2.webp")))
        list.add(GameData(43, getFromAssets(context,"imgs/forest3.webp")))
        list.add(GameData(44, getFromAssets(context,"imgs/forest4.webp")))
        list.add(GameData(45, getFromAssets(context,"imgs/forest5.webp")))
        list.add(GameData(46, getFromAssets(context,"imgs/forest6.webp")))
        list.add(GameData(47, getFromAssets(context,"imgs/forest7.webp")))
        list.add(GameData(48, getFromAssets(context,"imgs/forest8.webp")))
        list.add(GameData(49, getFromAssets(context,"imgs/fruit1.webp")))
        list.add(GameData(50, getFromAssets(context,"imgs/fruit2.webp")))
        list.add(GameData(51, getFromAssets(context,"imgs/fruit3.webp")))
        list.add(GameData(52, getFromAssets(context,"imgs/fruit4.webp")))
        list.add(GameData(53, getFromAssets(context,"imgs/fruit5.webp")))
        list.add(GameData(54, getFromAssets(context,"imgs/fruit6.webp")))
        list.add(GameData(55, getFromAssets(context,"imgs/fruit7.webp")))
        list.add(GameData(56, getFromAssets(context,"imgs/fruit8.webp")))
        list.add(GameData(57, getFromAssets(context,"imgs/insect1.webp")))
        list.add(GameData(58, getFromAssets(context,"imgs/insect2.webp")))
        list.add(GameData(59, getFromAssets(context,"imgs/insect3.webp")))
        list.add(GameData(60, getFromAssets(context,"imgs/insect4.webp")))
        list.add(GameData(61, getFromAssets(context,"imgs/insect5.webp")))
        list.add(GameData(62, getFromAssets(context,"imgs/insect6.webp")))
        list.add(GameData(63, getFromAssets(context,"imgs/insect7.webp")))
        list.add(GameData(64, getFromAssets(context,"imgs/insect8.webp")))
        list.add(GameData(65, getFromAssets(context,"imgs/nature1.webp")))
        list.add(GameData(66, getFromAssets(context,"imgs/nature2.webp")))
        list.add(GameData(67, getFromAssets(context,"imgs/nature3.webp")))
        list.add(GameData(68, getFromAssets(context,"imgs/nature4.webp")))
        list.add(GameData(69, getFromAssets(context,"imgs/nature5.webp")))
        list.add(GameData(70, getFromAssets(context,"imgs/nature6.webp")))
        list.add(GameData(71, getFromAssets(context,"imgs/nature7.webp")))
        list.add(GameData(72, getFromAssets(context,"imgs/nature8.webp")))
        list.add(GameData(73, getFromAssets(context,"imgs/transport1.webp")))
        list.add(GameData(74, getFromAssets(context,"imgs/transport2.webp")))
        list.add(GameData(75, getFromAssets(context,"imgs/transport3.webp")))
        list.add(GameData(76, getFromAssets(context,"imgs/transport4.webp")))
        list.add(GameData(77, getFromAssets(context,"imgs/transport5.webp")))
        list.add(GameData(78, getFromAssets(context,"imgs/transport6.webp")))
        list.add(GameData(79, getFromAssets(context,"imgs/transport7.webp")))
        list.add(GameData(80, getFromAssets(context,"imgs/transport8.webp")))
    }

    override suspend fun getDataByLevel(level: LevelEnum): List<GameData> {
        val count = level.x * level.y
        list.shuffle()
        return list.subList(0, count / 2)
    }

    override fun getLevel() = pref.level

    override fun setLevel(level: Int) {
        pref.level = level
    }

    override fun getAttempts(): Int = pref.attempt

    override fun setAttempts(attempt: Int) {
        pref.attempt = attempt
    }

    override fun isMusicOn(): Boolean = pref.isMusicOn

    override fun isMusicOn(bool: Boolean) {
        pref.isMusicOn = bool
    }
}