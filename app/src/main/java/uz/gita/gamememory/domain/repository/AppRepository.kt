package uz.gita.gamememory.domain.repository

import uz.gita.gamememory.data.model.GameData
import uz.gita.gamememory.data.model.LevelEnum

interface AppRepository {

   suspend fun getDataByLevel(level: LevelEnum): List<GameData>

   fun getLevel(): Int

   fun setLevel(level: Int)

   fun getAttempts(): Int

   fun setAttempts(attempt: Int)

   fun isMusicOn(): Boolean

   fun isMusicOn(bool: Boolean)
}