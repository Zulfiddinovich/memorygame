package uz.gita.gamememory.domain.usecase.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import uz.gita.gamememory.data.model.GameData
import uz.gita.gamememory.data.model.LevelEnum
import uz.gita.gamememory.domain.repository.AppRepository
import uz.gita.gamememory.domain.usecase.AllDataUseCase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AllDataUseCaseImpl @Inject constructor(
    private val repository: AppRepository
) : AllDataUseCase {

    override fun getDataByLevel(level: LevelEnum): Flow<List<GameData>> = flow {
        val result = ArrayList<GameData>()
        val demo = repository.getDataByLevel(level)

        result.addAll(demo)
        result.addAll(demo)

        result.shuffle()

        emit(result)
    }.flowOn(Dispatchers.IO)

    override fun getLevel(): Int = repository.getLevel()

    override fun setLevel(level: Int) = repository.setLevel(level)

    override fun getAttempts(): Int = repository.getAttempts()

    override fun setAttempts(attempt: Int) = repository.setAttempts(attempt)

    override fun isMusicOn(): Boolean = repository.isMusicOn()

    override fun isMusicOn(bool: Boolean) = repository.isMusicOn(bool)
}