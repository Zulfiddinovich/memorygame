package uz.gita.gamememory.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import timber.log.Timber
import java.io.IOException
import java.io.InputStream

fun <T> T.scope(block: T.() -> Unit) {
    block(this)
}

fun myLog(message: String, tag: String = "TTT") {
    Timber.tag(tag).d(message)
}

fun View.gone() {
    this.visibility = View.INVISIBLE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun getFromAssets(context: Context, imageName: String): Drawable {
    var drawable: Drawable? = null
    try {
        // get input stream
        val ims: InputStream = context.assets.open(imageName)    // "avatar.jpg"
        // load image as Drawable
        drawable = Drawable.createFromStream (ims, null)
        // set image to ImageView
        // mImage.setImageDrawable(d)
        ims.close()
    } catch (e: IOException) {
        // toast
    }
    return drawable!!
}